ARG UBI_VERSION=8.8-1067.1696517599
FROM registry.access.redhat.com/ubi8/ubi:$UBI_VERSION

RUN dnf install -y curl git sudo \
  && dnf clean all \
  && groupadd coder \
  && useradd -g coder coder \
  && echo '%coder  ALL=(ALL)       NOPASSWD: ALL' >> /etc/sudoers

COPY etc/wsl.conf /etc/