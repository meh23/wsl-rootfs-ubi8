wsl-rootfs-ubi8
===============

docker image with ubi8 rootfs for importing as wsl distro

## Pre-requisite
* [WSL2](https://learn.microsoft.com/en-us/windows/wsl/install)

## Import
1. [download](/../-/jobs/artifacts/main/download?job=build) and extract artifacts from latest job

2. install wsl distro
   ```powershell
   PS> Set-ExecutionPolicy Bypass -Scope Process -Force; .\install.ps1
   ```