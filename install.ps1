# Specify the name of the WSL distribution
$distroName = "ubi8"

# Specify the folder name you want to check/create
$distroFolderName = Join-Path -Path "wsldistro" -ChildPath $distroName

# Get the path to the user's home directory
$userHome = [Environment]::GetFolderPath("UserProfile")

# Construct the full path to the folder
$distroFolderPath = Join-Path -Path $userHome -ChildPath $distroFolderName

# Check if the folder exists
if (-not (Test-Path -Path $distroFolderPath -PathType Container)) {
    # Folder does not exist, create it
    New-Item -Path $distroFolderPath -ItemType Directory
    Write-Host "Folder '$distroFolderName' created in $userHome."
} else {
    Write-Host "Folder '$distroFolderName' already exists in $userHome."
}

# Get the current directory where the script is located
$currentDirectory = Get-Location

# Construct the full path to the tar file (assuming it's named "ubi8.tar")
$tarFilePath = Join-Path -Path $currentDirectory -ChildPath "ubi8.tar"

# Check if the distribution already exists
$existingDistro = wsl --list --quiet | Where-Object { $_ -eq $distroName }

if ($existingDistro) {
    # Distribution exists, terminate and unregister it
    wsl --terminate $distroName
    Write-Host "Existing WSL distribution '$distroName' terminated."
    wsl --unregister $distroName
    Write-Host "Existing WSL distribution '$distroName' unregistered."
}

# Distribution does not exist, import it
wsl --import $distroName $distroFolderPath $tarFilePath
Write-Host "WSL distribution '$distroName' imported from '$tarFilePath'."
